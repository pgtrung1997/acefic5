import dynamic from "next/dynamic";
import React from "react";

const ContactPage = dynamic(() => import("../src/container/ContactPage"), {
  ssr: false,
});

const contact = () => {
  return <ContactPage />;
};

export default contact;
