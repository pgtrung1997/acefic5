import { useEffect } from "react";
import AOS from "aos";
import "../src/shared/styles/globals.css";
import type { AppProps } from "next/app";
import "antd/dist/antd.css";
import "aos/dist/aos.css";
import { Provider } from "react-redux";
import store from "../src/shared/store/store";

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    AOS.init({
      duration: 1000,
      once: true,
    });
  }, []);

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
