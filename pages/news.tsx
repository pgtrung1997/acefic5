import dynamic from "next/dynamic";
import React from "react";

const NewsPage = dynamic(() => import("../src/container/NewsPage"), {
  ssr: false,
});

const news = () => {
  return <NewsPage />;
};

export default news;
