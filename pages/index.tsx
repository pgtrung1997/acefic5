import { NextPage } from "next";
import dynamic from "next/dynamic";

const HomePage = dynamic(() => import("../src/container/HomePage"), {
  ssr: false,
});

const Home: NextPage = () => {
  return <HomePage />;
};

export default Home;
