import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import React from "react";

const NewsDetailPage = dynamic(
  () => import("../../src/container/NewsDetailPage"),
  { ssr: false }
);

const Post = () => {
  const router = useRouter();
  const { postId } = router.query;

  return <NewsDetailPage postId={postId} />;
};

export default Post;
