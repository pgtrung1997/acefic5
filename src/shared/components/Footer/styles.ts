import styled from "styled-components";
import { FooterProps } from ".";
import theme from "../../styles/theme";

export const Container = styled.div<FooterProps>`
  width: 100%;
  background: ${(props) => (props.sidepage ? "transparent" : theme.blue)};
  position: relative;
  padding: ${(props) => (props.sidepage ? "39px 0  0" : "79px 0 23px")};
  margin-top: ${(props) => (props.sidepage ? "68px" : "53px")};
`;
export const Wrapper = styled.div`
  max-width: 1100px;
  margin: auto;
  display: flex;
  justify-content: space-around;

  @media ${theme.laptopL} {
    max-width: 1100px;
  }
  @media ${theme.laptopM} {
    max-width: 850px;
  }
  @media ${theme.laptop} {
    max-width: 700px;
  }
  @media ${theme.tablet} {
    padding: 0 30px;
  }
  @media ${theme.mobile} {
    flex-direction: column;
  }
`;
export const FooterLeft = styled.div<FooterProps>`
  display: flex;
  flex: 50%;
  justify-content: flex-end;
  align-items: flex-start;
  padding-left: ${(props) => (props.sidepage ? "157px" : "173px")};
  margin-top: ${(props) => (props.sidepage ? "22px" : "0px")};
  @media ${theme.laptopM} {
    padding-left: 0;
  }
`;
export const Services = styled.ul`
  font: 700 25.02px ${theme.OpenSans};
  text-transform: uppercase;
  text-decoration: underline;
  color: ${theme.white};
  margin: 26px 0 35px;
  @media ${theme.mobile} {
    padding: 0;
    margin-right: 10px;
  }
`;
export const ServiceList = styled.ul`
  @media ${theme.mobile} {
    padding: 0;
    margin-right: 10px;
  }
`;
export const ServicesItem = styled.li`
  font: 14.58px ${theme.OpenSans};
  color: ${theme.white};
  list-style: none;
  margin-bottom: 22px;
`;
export const FooterBackground = styled.div`
  position: relative;
  margin-top: 53px;
`;
export const MyImage = styled.div<FooterProps>`
  display: ${(props) => (props.sidepage ? "" : "none")};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: 100%;
  z-index: -1;
  overflow: hidden;
  span {
    min-width: 100%;
    min-height: 100%;
  }
`;
export const Copyright = styled.div`
  max-width: 1100px;
  background: ${theme.blue};
  margin: auto;
  text-align: center;
  margin-top: -5px;
  padding: 21px 0 24px;
`;
