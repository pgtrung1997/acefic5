import React from "react";
import logo from "../../../../public/Layer 22.png";
import logoWhite from "../../../../public/Layer22-white.png";
import Image from "next/image";
import footerBackground from "../../../../public/Rectangle 10.jpg";
import theme from "../../styles/theme";
import { Left, Para, Right } from "../../styles/theme/common";
import {
  Container,
  Copyright,
  FooterLeft,
  MyImage,
  ServiceList,
  Services,
  ServicesItem,
  Wrapper,
} from "./styles";

export interface FooterProps {
  sidepage?: boolean;
}

const Footer = (props: FooterProps) => {
  const { sidepage } = props;
  return (
    <>
      <Container sidepage={sidepage ? true : false}>
        <MyImage sidepage={sidepage ? true : false}>
          <Image src={footerBackground}></Image>
        </MyImage>
        <Wrapper>
          <Right flex="20%">
            {sidepage ? (
              <Image src={logoWhite}></Image>
            ) : (
              <Image src={logo}></Image>
            )}

            <Para
              family="UTMItalic"
              color={theme.white}
              size={sidepage ? "14.58px" : "12.5px"}
              margin={sidepage ? "32px 0 32px" : "14px 0 32px"}
              lineHeight={sidepage ? "1.7" : "1.65"}
            >
              ACEFIC5 là Nhà thầu chính thi công tại các dự án lớn tại Hà Nội,
              Đà Nẵng và nhiều tỉnh thành thành phố trên cả nước. Công ty Cổ
              phần Đầu tư và Xây dựng ACE5 Thái Bình Dương luôn đáp ứng được các
              yêu cầu khắt khe của các Chủ đầu tư, luôn đem đến cho khách hàng
              và xã hội các sản phẩm với chất lượng tốt nhất, tiến độ nhanh nhất
              mà vẫn đảm bảo tính an toàn và hiệu quả đầu tư trên mỗi công
              trình...
            </Para>
            {sidepage ? (
              ""
            ) : (
              <Para family={theme.OpenSans} color={theme.white} size="14.6px">
                Copyright 2019 Acefic5. All Rights Reserved
              </Para>
            )}
          </Right>
          <FooterLeft sidepage={sidepage ? true : false}>
            <Right>
              <Services>Dịch vụ</Services>
              <ServiceList>
                <ServicesItem>Privacy Policy</ServicesItem>
                <ServicesItem>Terms and Conditions</ServicesItem>
                <ServicesItem>Copyright Policy</ServicesItem>
                <ServicesItem>Code of Conduct</ServicesItem>
              </ServiceList>
            </Right>
            <Left>
              <Services>Dịch vụ</Services>
              <ServiceList>
                <ServicesItem>Chemical Engineering Projects</ServicesItem>
                <ServicesItem>Mining Engineering Construction</ServicesItem>
                <ServicesItem>Engineering Welding Engineering</ServicesItem>
                <ServicesItem>Welding Engineering</ServicesItem>
              </ServiceList>
            </Left>
          </FooterLeft>
        </Wrapper>
        {sidepage ? (
          <Copyright>
            <Para
              family={theme.OpenSans}
              color={theme.white}
              size="14.6px"
              margin="0"
            >
              Bản quyền &copy; 2019 bởi Công ty Cổ phần Đầu tư và Xây dựng ACE5
              Thái Bình Dương
            </Para>
          </Copyright>
        ) : (
          ""
        )}
      </Container>
    </>
  );
};

export default Footer;
