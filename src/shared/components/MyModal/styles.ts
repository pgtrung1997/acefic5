import styled from "styled-components";
import theme from "../../styles/theme";

export const ProjectTitle = styled.h1`
  font-family: ${theme.OpenSans};
  font-size: 25px;
  font-weight: 700;
  text-transform: uppercase;
  text-align: center;
  margin-bottom: 37px;
  color: ${theme.blue};
`;
export const DetailContainer = styled.div`
  max-width: 652px;
  margin: auto;
  border-radius: 10px;
  position: relative;
  background: ${theme.white};
  top: -29px;
  padding: 22px 0;
  @media ${theme.tablet} {
    position: initial;
    max-width: 100%;
  }
`;
export const DetailWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: -4px;
`;
export const DetailName = styled.h6`
  flex: 29%;
  font-family: ${theme.OpenSans};
  font-size: 14.58px;
  font-weight: 700;
  padding-left: 11px;
`;
export const DetailDesc = styled.p`
  flex: 70%;
  font-family: ${theme.OpenSans};
  font-size: 14.58px;
`;
