import { Modal } from "antd";
import React from "react";
import Image from "next/image";
import projectImg from "../../../../public/N04B_phoicanh.jpg";
import {
  DetailContainer,
  DetailDesc,
  DetailName,
  DetailWrapper,
  ProjectTitle,
} from "./styles";

interface MyModalProps {
  isModalVisible: boolean;
  handleCancel: () => void;
}

const MyModal = (props: MyModalProps) => {
  const { isModalVisible, handleCancel } = props;
  return (
    <>
      <Modal
        title="    "
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={null}
        width="810px"
      >
        <Image src={projectImg} layout="responsive"></Image>
        <DetailContainer>
          <ProjectTitle>Dự án N04B - Ngoại giao đoàn</ProjectTitle>
          <DetailWrapper>
            <DetailName>Tên dự án</DetailName>
            <DetailDesc>
              : Tổ hợp chung cư cao tầng N04B - Khu đoàn Ngoại giao tại Hà Nội
            </DetailDesc>
          </DetailWrapper>
          <DetailWrapper>
            <DetailName>Chủ đầu tư</DetailName>
            <DetailDesc>
              : Công ty Cổ phần Đầu tư Xây dựng Bất động sản Lanmark
            </DetailDesc>
          </DetailWrapper>
          <DetailWrapper>
            <DetailName>Hạng mục</DetailName>
            <DetailDesc>
              : Phần kết cấu thân nhà / Phần hoàn thiện cấu trúc
            </DetailDesc>
          </DetailWrapper>
          <DetailWrapper>
            <DetailName>Địa điểm</DetailName>
            <DetailDesc>
              : Khu đoàn Ngoại giao, Xuân Tảo, Bắc Từ Liêm, Hà Nội
            </DetailDesc>
          </DetailWrapper>
          <DetailWrapper>
            <DetailName>Loại công trình</DetailName>
            <DetailDesc>: Dân dụng, căn hộ chung cư</DetailDesc>
          </DetailWrapper>
          <DetailWrapper>
            <DetailName>Cấp công trình</DetailName>
            <DetailDesc>: Cấp 1</DetailDesc>
          </DetailWrapper>
          <DetailWrapper>
            <DetailName>Thời gian thực hiện</DetailName>
            <DetailDesc>: 2013 - 2015</DetailDesc>
          </DetailWrapper>
        </DetailContainer>
      </Modal>
    </>
  );
};

export default MyModal;
