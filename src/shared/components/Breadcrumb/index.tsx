import Link from "next/link";
import React from "react";
import {
  BreadcrumbContainer,
  BreadcrumbLink,
  BreadcrumbTitle,
  CurrentRoute,
  LinkContainer,
} from "./styles";

interface BreadcrumbProps {
  routing: string;
}

const Breadcrumb = (props: BreadcrumbProps) => {
  const { routing } = props;
  return (
    <>
      <BreadcrumbContainer>
        <BreadcrumbTitle>{routing}</BreadcrumbTitle>
        <LinkContainer>
          <Link href="/">
            <BreadcrumbLink>Trang chủ </BreadcrumbLink>
          </Link>
          <CurrentRoute>/ {routing}</CurrentRoute>
        </LinkContainer>
      </BreadcrumbContainer>
    </>
  );
};

export default Breadcrumb;
