import styled from "styled-components";
import theme from "../../styles/theme";

export const BreadcrumbTitle = styled.h2`
  font: 700 25px ${theme.OpenSans};
  color: ${theme.blue};
  text-transform: uppercase;
  margin-bottom: 10px;
`;
export const BreadcrumbContainer = styled.div`
  background: ${theme.water};
  text-align: center;
  padding: 44px 0 40px;
`;
export const LinkContainer = styled.div``;
export const BreadcrumbLink = styled.a`
  font: 14.58px ${theme.OpenSans};
  color: ${theme.blue};
`;
export const CurrentRoute = styled.span`
  font: 14.58px ${theme.OpenSans};
  color: ${theme.blue};
`;
