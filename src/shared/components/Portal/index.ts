import React, { useEffect, useState } from "react";
import { createPortal } from "react-dom";

interface PortalProps {
  children: any;
  selector: any;
}

const Portal = ({ children, selector }: PortalProps) => {
  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setMounted(true);
  }, [selector]);
  return mounted
    ? createPortal(children, document.querySelector(selector) as HTMLElement)
    : null;
};

export default Portal;
