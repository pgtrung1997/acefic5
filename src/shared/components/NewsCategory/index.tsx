import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import image from "../../../../public/Rounded Rectangle 34.png";
import Image from "next/image";
import clock from "../../../../public/Layer 36.png";
import theme from "../../styles/theme";
import { Left, NewsPara, Right } from "../../styles/theme/common";
import {
  Highlight,
  HighLightContainer,
  HighLightItem,
  HighLightList,
  InputContainer,
  NewsCategoryContainer,
  SearchingInput,
  Date,
} from "./styles";

const NewsCategory = () => {
  const list = [1, 2, 3, 4, 5];
  return (
    <NewsCategoryContainer>
      <InputContainer>
        <SearchingInput type="text" placeholder="Tìm kiếm"></SearchingInput>
        <FontAwesomeIcon icon={faSearch} color={theme.green} size="lg" />
      </InputContainer>
      <HighLightContainer>
        <Highlight>Tin nổi bật</Highlight>
        <HighLightList>
          {list.map((v: number) => {
            return (
              <HighLightItem key={v}>
                <Left flex="36%">
                  <Image src={image} layout="responsive"></Image>
                </Left>
                <Right
                  flex="70%"
                  padding="0 0 0 18px"
                  paddinglaptopM="0"
                  marginlaptopM="10px 0 0"
                >
                  <NewsPara
                    family={theme.OpenSans}
                    size="12.5px"
                    lineHeight="1.3"
                  >
                    Far concluded not his something extremity.
                  </NewsPara>
                  <Date>
                    <Image src={clock}></Image>
                    <NewsPara
                      family={theme.OpenSans}
                      size="11.67px"
                      color={theme.gray}
                      margin="0 0 0 7px"
                    >
                      5. december.2020
                    </NewsPara>
                  </Date>
                </Right>
              </HighLightItem>
            );
          })}
        </HighLightList>
      </HighLightContainer>
    </NewsCategoryContainer>
  );
};

export default NewsCategory;
