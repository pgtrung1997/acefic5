import styled from "styled-components";
import theme from "../../styles/theme";

export const SearchingInput = styled.input`
  border: 1px solid ${theme.gray};
  background: ${theme.titanWhite};
  min-width: 100%;
  padding: 13px 20px;
  ::placeholder {
    font: italic 14.58px ${theme.OpenSans};
  }
`;
export const InputContainer = styled.div`
  position: relative;
  svg {
    position: absolute;
    top: 50%;
    right: 20px;
    transform: translateY(-50%);
  }
`;
export const NewsCategoryContainer = styled.div`
  margin-left: 28px;
  @media ${theme.laptopM} {
    margin-left: 0;
  }
`;
export const Highlight = styled.h4`
  font: 16.67px "Myriad";
  text-transform: uppercase;
  border-bottom: 2px solid ${theme.green};
  padding-bottom: 10px;
`;
export const HighLightContainer = styled.div`
  background: ${theme.titanWhite};
  margin-top: 33px;
  padding: 12px 19px 7px;
`;
export const HighLightList = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  @media ${theme.laptopM} {
    flex-direction: row;
  }
  @media ${theme.tablet} {
    justify-content: space-between;
  }
`;
export const HighLightItem = styled.div`
  display: flex;
  margin: 11px 0;
  @media ${theme.laptopM} {
    flex-direction: column;
    width: calc(30% - 26px);
    margin: 15px 26px;
  }
  @media ${theme.laptop} {
    width: calc(30% - 21px);
    margin: 10px 21px;
  }
  @media ${theme.tablet} {
    width: 45%;
    margin: 10px 0;
  }
`;
export const Date = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;
`;
