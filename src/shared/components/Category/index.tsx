import React from "react";
import theme from "../../styles/theme";
import {
  CategoryItem,
  LinearBackground,
  MarketPara,
  Order,
  SubTitle,
} from "./styles";

interface CategoryProps {
  index: string;
  title: string;
}

const Category = (prop: CategoryProps) => {
  const { index, title } = prop;
  return (
    <CategoryItem>
      <Order>{index}</Order>
      <SubTitle>{title}</SubTitle>
      <LinearBackground margin="-12px 0 -32px 45px">
        <MarketPara
          family={theme.OpenSans}
          size="13.33px"
          weight="500"
          color={theme.blue}
        >
          Scarcely on striking packages by so property in delicate. Up or well
          must less rent read walk so be. Easy sold at do hour sing spot.
        </MarketPara>
      </LinearBackground>
    </CategoryItem>
  );
};

export default Category;
