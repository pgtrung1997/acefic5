import styled from "styled-components";
import theme from "../../styles/theme";

interface StylesProps {
  margin?: string;
  family?: string;
  size?: string;
  weight?: string;
}

export const SubTitle = styled.h4`
  font-family: ${theme.OpenSans};
  font-size: 16.67px;
  font-weight: 700;
  text-transform: uppercase;
  display: inline-block;
  color: ${theme.blue};
`;
export const Order = styled.span`
  font-family: "SFUDax";
  font-size: 62.5px;
  color: ${theme.lightGray};
  position: relative;
  left: 14px;
  top: 6px;
  z-index: -1;
`;
export const LinearBackground: any = styled.div<StylesProps>`
  background: linear-gradient(
    146deg,
    rgba(42, 241, 158, 1) 22%,
    rgba(10, 178, 232, 1) 40%,
    rgba(255, 255, 255, 1) 59%
  );
  margin: ${(props) => props.margin};
  padding: 1px;
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s ease-out;
  position: relative;
  left: 0;
  &:hover {
    left: -20px;
    background: none;
    box-shadow: 5px 5px 20px rgba(23, 48, 92, 0.2);
    p {
      background: linear-gradient(
        90deg,
        rgba(141, 233, 199, 1) 0%,
        rgba(141, 233, 199, 0.5) 50%,
        rgba(141, 233, 199, 0) 100%
      );
    }
  }
`;
export const MarketPara: any = styled.p<StylesProps>`
  font-weight: ${(props) => props.weight};
  font-size: ${(props) => props.size};
  font-family: ${(props) => props.family};
  color: ${(props) => props.color};
  line-height: 1.6;
  margin: 0;
  background: ${theme.white};
  border-radius: 5px;
  padding: 7px 9px;
  transition: 1s ease-out;
`;
export const CategoryItem = styled.div`
  margin-bottom: -26px;
`;
