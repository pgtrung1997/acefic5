import styled from "styled-components";
import { NewsHeaderProps } from ".";
import theme from "../../styles/theme";

export const NewsDetailHeader: any = styled.div<NewsHeaderProps>`
  display: flex;
  align-items: center;
  justify-content: ${(props) => (props.start ? "flex-start" : "center")};
  flex-wrap: wrap;
  @media ${theme.tablet} {
  }
`;
