import React from "react";
import theme from "../../styles/theme";
import Image from "next/image";
import clock from "../../../../public/Layer 36.png";
import divider from "../../../../public/Rectangle 259.png";
import eye from "../../../../public/Layer 37.png";
import { Margin, NewsPara } from "../../styles/theme/common";
import { NewsDetailHeader } from "./styles";

export interface NewsHeaderProps {
  start?: boolean;
  date?: string;
  view?: string;
  upload: string;
  popular: number;
}

const NewsHeader = ({
  start,
  date,
  view,
  upload,
  popular,
}: NewsHeaderProps) => {
  return (
    <NewsDetailHeader start={start ? true : false}>
      <Image src={clock}></Image>
      <NewsPara
        family={theme.OpenSans}
        size={date ? date : "12.5px"}
        color={theme.gray}
        margin="0 10px"
      >
        {upload}
      </NewsPara>
      <Image src={divider}></Image>
      <Margin margin="0 10px 0 20px">
        <Image src={eye}></Image>
      </Margin>
      <NewsPara
        family={theme.Montserrat}
        size={view ? view : "11.67px"}
        color={theme.gray}
      >
        {popular}
      </NewsPara>
    </NewsDetailHeader>
  );
};

export default NewsHeader;
