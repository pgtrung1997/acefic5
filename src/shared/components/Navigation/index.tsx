import React from "react";
import { NaviContain, PrevNext, Number, NumberText, Text } from "./styles";

const Navigation = () => {
  return (
    <NaviContain>
      <PrevNext>
        <Text>Prev</Text>
      </PrevNext>
      <Number>
        <NumberText>1</NumberText>
      </Number>
      <Number>
        <NumberText>2</NumberText>
      </Number>
      <Number>
        <NumberText>3</NumberText>
      </Number>
      <Number>
        <NumberText>60</NumberText>
      </Number>
      <PrevNext>
        <Text>Next</Text>
      </PrevNext>
    </NaviContain>
  );
};

export default Navigation;
