import styled from "styled-components";
import theme from "../../styles/theme";

export const Number = styled.div`
  border-radius: 50px;
  border: 2px solid ${theme.lavender};
  padding: 19px;
  display: inline-block;
  position: relative;
  margin: 0 6px;
  cursor: pointer;
`;
export const NumberText = styled.span`
  font-family: ${theme.OpenSans};
  font-size: 14px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
export const PrevNext = styled.div`
  border-radius: 50px;
  background: ${theme.lavender};
  padding: 21px 44px;
  display: inline-block;
  position: relative;
  cursor: pointer;
`;
export const Text = styled.p`
  font-family: ${theme.OpenSans};
  font-size: 14px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
export const NaviContain = styled.div`
  margin-top: 21px;
  text-align: center;
`;
