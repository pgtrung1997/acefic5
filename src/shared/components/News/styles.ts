import styled from "styled-components";
import theme from "../../styles/theme";

interface StylesProps {
  center?: boolean;
  family: string;
  size: string;
  color: string;
  margin?: string;
  weight?: string;
  lineHeight?: string;
  toplaptopM?: string;
  leftlaptopM?: string;
  rightlaptopM?: string;
  displaylaptop: boolean;
  sizelaptop: string;
  toplaptop: string;
  leftlaptop: string;
  rightlaptop: string;
}

export const NewsDetail: any = styled.div<StylesProps>`
  background: ${theme.white};
  padding: 11px 17px 20px;
  border-radius: 5px;
  position: absolute;
  top: 213px;
  left: 25px;
  right: 25px;
  transition: 0.3s;
  @media ${theme.laptopM} {
    top: ${(props) => props.toplaptopM};
    left: ${(props) => props.leftlaptopM};
    right: ${(props) => props.rightlaptopM};
  }
  @media ${theme.laptop} {
    top: ${(props) => props.toplaptop};
    left: ${(props) => props.leftlaptop};
    right: ${(props) => props.rightlaptop};
    box-shadow: 0 28px 35px rgba(131, 151, 185, 0.18);
  }
  @media ${theme.tablet} {
    position: initial;
  }
`;
export const NewsContainer: any = styled.div<StylesProps>`
  text-align: center;
  position: relative;
  flex: 30%;
  margin: ${(props) => (props.center ? "0 25px" : "")};
  cursor: pointer;
  &:hover {
    ${NewsDetail} {
      box-shadow: 0 28px 35px rgba(131, 151, 185, 0.18);
    }
  }
  @media ${theme.laptop} {
    display: ${(props) => (props.displaylaptop ? "none" : "")};
    margin: 0 10px;
  }
  @media (max-width: 600px) {
    margin: 0 0 50px;
  }
`;
export const NewsDetailHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media ${theme.tablet} {
  }
`;
export const NewsDetailFooter = styled.div`
  display: flex;
  align-items: center;
`;

export const NewsButton = styled.button`
  background: ${theme.green};
  border: none;
  border-radius: 3px;
  margin-left: 10px;
  padding: 8px 14px;
  box-shadow: 10px 10px 25px rgba(31, 211, 146, 0.45);
`;
export const NewsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: wrap;
  @media (max-width: 600px) {
    flex-direction: column;
    align-items: center;
  }
`;
