import React from "react";
import Image from "next/image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import NewsHeader from "../NewsHeader";
import theme from "../../styles/theme";
import { NewsPara } from "../../styles/theme/common";
import {
  NewsButton,
  NewsContainer,
  NewsDetail,
  NewsDetailFooter,
  NewsWrapper,
} from "./styles";
import { IPost } from "../../../container/HomePage/interface";

interface NewsProps {
  data?: IPost[];
}

const News = ({ data }: NewsProps) => {
  return (
    <>
      <NewsWrapper>
        {data?.map((v: IPost, i: number) => {
          if (i < 3) {
            return (
              <NewsContainer
                center={v.id === 2 ? true : false}
                displaylaptop={v.id === 3 ? true : false}
                key={i}
              >
                {v.imgsm ? (
                  <Image
                    src={v.imgsm}
                    layout="responsive"
                    width={363.33}
                    height={296.7}
                  ></Image>
                ) : (
                  ""
                )}

                <NewsDetail
                  toplaptopM="150px"
                  rightlaptopM="10px"
                  leftlaptopM="10px"
                  toplaptop="200px"
                >
                  <NewsHeader upload={v.date} popular={v.view} />
                  <NewsPara
                    family={theme.OpenSans}
                    weight="600"
                    color={theme.black}
                    size="20.83px"
                    margin="10px 0 22px"
                    lineHeight="1.2"
                    sizelaptop="18px"
                  >
                    Lorem ipsum dolor sit amet aenean nisi sociis ipsum...
                  </NewsPara>
                  <NewsDetailFooter>
                    <NewsPara
                      family={theme.OpenSans}
                      weight="600"
                      color={theme.black}
                      size="14.58px"
                    >
                      Xem thêm
                    </NewsPara>
                    <NewsButton>
                      <FontAwesomeIcon icon={faPlus} color={theme.white} />{" "}
                    </NewsButton>
                  </NewsDetailFooter>
                </NewsDetail>
              </NewsContainer>
            );
          }
          return;
        })}
      </NewsWrapper>
    </>
  );
};

export default News;
