import styled from "styled-components";
import theme from "../../styles/theme";
import Image from "next/image";

export const SampleNextArrow = styled.div`
  visibility: hidden;
  &:before {
    font-family: none;
  }
  &::before {
    background: ${theme.green};
    border-radius: 50%;
    visibility: visible;
    padding: 7px;
    position: relative;
    top: -348px;
    right: 74px;
    box-shadow: 7px 7px 20px rgba(31, 210, 145, 0.55);
    @media ${theme.laptopL} {
      right: 51px;
    }
    @media ${theme.laptop} {
      display: none;
    }
  }
`;
export const SamplePrevArrow = styled.div`
  visibility: hidden;
  &:before {
    font-family: none;
  }
  &::before {
    background: ${theme.green};
    border-radius: 50%;
    visibility: visible;
    padding: 7px;
    position: relative;
    top: -348px;
    right: -812px;
    box-shadow: 7px 7px 20px rgba(31, 210, 145, 0.55);
    @media ${theme.laptopM} {
      right: -560px;
    }
    @media ${theme.laptop} {
      display: none;
    }
  }
`;
export const OverlayContainer = styled.div`
  background: ${theme.white};
  position: absolute;
  top: 235px;
  left: 20px;
  right: 20px;
  padding: 10px 12px;
  display: none;
  text-align: start;
  @media ${theme.laptopM} {
    right: 30px;
    left: 30px;
  }
  @media ${theme.laptop} {
    top: 225px;
    right: 10px;
    left: 10px;
    padding: 5px;
  }
  @media ${theme.tablet} {
    right: 10px;
    left: 10px;
    top: 190px;
    padding: 10px 30px 0;
  }
  @media ${theme.mobile} {
    top: 331px;
  }
`;
export const MyImage = styled(Image)`
  transition: 0.3s;
`;
export const Title = styled.h6`
  font: 14.58px ${theme.OpenSans};
  color: ${theme.green};
  text-transform: uppercase;
  margin: 0;
`;
export const Category = styled.p`
  font: 12.5px ${theme.OpenSans};
  margin: 0;
  @media ${theme.tablet} {
    margin-bottom: 20px;
  }
`;
export const ProjectContainer = styled.div`
  position: relative;
  cursor: pointer;
  transition: 0.3s;
  margin: 11px 15px 11px;
  text-align: center;
  :hover {
    transform: translateY(-15px);
    ${OverlayContainer} {
      display: block;
    }
  }
  @media ${theme.laptopM} {
    margin: 11px 25px 11px;
  }
  @media ${theme.laptop} {
    margin: 11px 3px 11px;
  }
  @media ${theme.tablet} {
    margin: 11px 0 0;
  }
`;
export const PlusButton = styled.button`
  background: ${theme.green};
  position: absolute;
  top: 50%;
  right: 10px;
  transform: translateY(-50%);
  border: none;
  color: ${theme.white};
  padding: 5px 10px;
  @media ${theme.tablet} {
    position: initial;
  }
`;
