import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import {
  Category,
  MyImage,
  OverlayContainer,
  PlusButton,
  ProjectContainer,
  SampleNextArrow,
  SamplePrevArrow,
  Title,
} from "./styles";
import { IProject } from "../../../container/HomePage/interface";
import { useSelector } from "react-redux";

interface MyCarouselProps {
  showModal: () => void;
}

const MyCarousel = ({ showModal }: MyCarouselProps) => {
  const { projectList } = useSelector((state: any) => state.HomepageData);
  const settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    rows: 2,
    className: "project",
    nextArrow: <SampleNextArrow></SampleNextArrow>,
    prevArrow: <SamplePrevArrow></SamplePrevArrow>,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          infinite: true,
          slidesToShow: 1,
          rows: 1,
          centerMode: true,
          centerPadding: "0px",
          variableWidth: true,
        },
      },
    ],
  };
  return (
    <>
      <Slider {...settings}>
        {projectList?.project?.map((v: IProject, i: number) => {
          return (
            <ProjectContainer key={i} onClick={showModal}>
              {v.img ? (
                <MyImage src={v.img} width={262} height={299}></MyImage>
              ) : (
                ""
              )}
              <OverlayContainer>
                <Title>Tòa nhà thành phố</Title>
                <Category>Thương mại</Category>
                <PlusButton>
                  <FontAwesomeIcon icon={faPlus} />
                </PlusButton>
              </OverlayContainer>
            </ProjectContainer>
          );
        })}
      </Slider>
    </>
  );
};

export default MyCarousel;
