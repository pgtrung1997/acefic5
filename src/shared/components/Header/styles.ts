import styled from "styled-components";
import theme from "../../styles/theme";

interface StylesProps {
  displaytablet?: string;
  sidepage?: boolean;
}

export const Wrapper = styled.div<StylesProps>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${theme.tablet} {
    display: ${(props) => props.displaytablet};
  }
`;
export const BorderBottom = styled.div<StylesProps>`
  border-bottom: 1px solid ${theme.gray};
  background: ${(props) => (props.sidepage ? theme.blue : "")};
`;
export const Input = styled.input<StylesProps>`
  border-radius: 50px;
  border: none;
  padding: 0px 15px;
  max-width: 175px;
  background: ${(props) => (props.sidepage ? theme.slateBlue : "")};
  &::placeholder {
    color: ${(props) => (props.sidepage ? theme.white : theme.blue)};
    font: 10px "UTMItalic";
  }
`;
export const Container = styled(Wrapper)`
  padding-top: 7px;
  padding-bottom: 7px;
  max-width: 1140px;
  margin: auto;
  @media ${theme.laptopL} {
    max-width: 1100px;
  }
  @media ${theme.laptopM} {
    max-width: 850px;
  }
  @media ${theme.laptop} {
    max-width: 100%;
    padding: 0 30px;
  }
`;
export const HeaderTop = styled(Container)`
  background: transparent;
  @media ${theme.laptop} {
    background: ${theme.blue};
    padding-top: 10px;
    padding-bottom: 10px;
  }
  @media ${theme.tablet} {
    justify-content: center;
  }
`;
export const HeaderNav = styled(Container)`
  padding-top: 17px;
  background: transparent;

  @media ${theme.laptop} {
    background: ${theme.white};
    padding: 10px 30px;
  }
`;
export const ContainerInput = styled.div`
  position: relative;
  svg {
    position: absolute;
    right: 10px;
    top: 50%;
    transform: translateY(-50%);
  }
`;
export const ListWrapper = styled.ul`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-right: -10px;
  transition: 0.3s;
  @media ${theme.laptop} {
    flex-direction: column;
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    z-index: 1;
    background: rgba(0, 0, 0, 0.5);
    margin: 0;
    width: 50%;
    height: 100%;
    padding: 50px 0;
  }
`;
export const ListItem = styled.li<StylesProps>`
  list-style: none;
  margin-left: 24px;
  padding: 6px 10px;
  transition: 0.3s;
  &:hover {
    background: ${theme.green};
  }
  a {
    color: ${(props) => (props.sidepage ? theme.blue : theme.white)};
    font-size: 14.58px;
    font-family: ${theme.OpenSans};
    text-transform: uppercase;
  }
  @media ${theme.laptopM} {
    margin-left: 0px;
    a {
      font-size: 13px;
    }
  }
  @media ${theme.laptop} {
    a {
      color: ${theme.white};
    }
  }
`;
export const Home = styled(ListItem)`
  background: ${theme.green};
`;
export const Hotline = styled.p<StylesProps>`
  font: italic 500 12px ${theme.Montserrat};
  color: ${(props) => (props.sidepage ? theme.white : theme.blue)};
  text-transform: uppercase;
  margin-left: 10px;
  margin-bottom: 0;
  @media ${theme.laptop} {
    color: ${theme.white};
  }
`;
export const LanguagePicker = styled(Wrapper)`
  background: ${(props) => (props.sidepage ? theme.slateBlue : theme.white)};
  padding: 7px 4px;
  margin: 0 17px;
  border-radius: 2px;
  svg {
    margin-left: 5px;
  }
`;
export const ImagePosition: any = styled.div`
  position: relative;
  top: -2px;
  @media ${theme.laptop} {
    max-width: 200px;
    position: initial;
  }
`;
export const BoxShadow = styled.div<StylesProps>`
  box-shadow: ${(props) =>
    props.sidepage ? "5px 5px 35px rgba(29,64,125,0.2)" : ""};
`;
