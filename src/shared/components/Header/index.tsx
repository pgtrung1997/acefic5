import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faBars,
  faPhoneFlip,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import Image from "next/image";
import Link from "next/link";
import vnFlag from "../../../../public/Layer3.png";
import logoCompany from "../../../../public/Layer4.png";
import theme from "../../styles/theme";
import {
  BorderBottom,
  BoxShadow,
  ContainerInput,
  HeaderNav,
  HeaderTop,
  Home,
  Hotline,
  ImagePosition,
  Input,
  LanguagePicker,
  ListItem,
  ListWrapper,
  Wrapper,
} from "./styles";

interface HeaderProps {
  sidepage?: boolean;
}

const Header = (props: HeaderProps) => {
  const { sidepage } = props;
  const [windowSize, setWindowSize] = useState<any>();
  const [navShow, setNavShow] = useState<boolean>(false);
  const [scroll, setScroll] = useState<boolean>(false);
  const Nav = () => {
    return (
      <ListWrapper>
        <Home>
          <Link href="/">Trang chủ</Link>
        </Home>
        <ListItem sidepage={sidepage ? true : false}>
          <Link href="/">Giới thiệu</Link>
        </ListItem>
        <ListItem sidepage={sidepage ? true : false}>
          <Link href="/">Lĩnh vực</Link>
        </ListItem>
        <ListItem sidepage={sidepage ? true : false}>
          <Link href="/">Dự án</Link>
        </ListItem>
        <ListItem sidepage={sidepage ? true : false}>
          <Link href="/news">Tin tức</Link>
        </ListItem>
        <ListItem sidepage={sidepage ? true : false}>
          <Link href="/">Tuyển dụng</Link>
        </ListItem>
        <ListItem sidepage={sidepage ? true : false}>
          <Link href="/contact">Liên hệ</Link>
        </ListItem>
      </ListWrapper>
    );
  };

  useEffect(() => {
    function handleResize() {
      setWindowSize(window.innerWidth);
      setNavShow(false);
    }
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  const changeBackground = () => {
    if (window.scrollY > 42) {
      setScroll(true);
    } else {
      setScroll(false);
    }
  };
  window.addEventListener("scroll", changeBackground);
  return (
    <>
      <BorderBottom sidepage={sidepage ? true : false} data-aos="fade-down">
        <HeaderTop>
          <Wrapper displaytablet="none">
            <FontAwesomeIcon icon={faPhoneFlip} color={theme.green} size="xs" />
            <Hotline sidepage={sidepage ? true : false}>
              Hotline: 1900 2863
            </Hotline>
          </Wrapper>
          <Wrapper>
            <ContainerInput>
              <Input
                type="text"
                name="search"
                id="search"
                placeholder="Search..."
                sidepage={sidepage ? true : false}
              />
              <FontAwesomeIcon icon={faSearch} color={theme.green} size="xs" />
            </ContainerInput>
            <LanguagePicker sidepage={sidepage ? true : false}>
              <Image src={vnFlag} />
              <FontAwesomeIcon
                icon={faAngleDown}
                size="xs"
                color={sidepage ? theme.white : theme.blue}
              />
            </LanguagePicker>
          </Wrapper>
        </HeaderTop>
      </BorderBottom>

      <BoxShadow
        sidepage={sidepage ? true : false}
        className={scroll ? "scrolled" : ""}
      >
        <HeaderNav>
          <ImagePosition>
            <Image src={logoCompany} />
          </ImagePosition>
          {windowSize <= 920 ? (
            <FontAwesomeIcon
              icon={faBars}
              size="lg"
              onClick={() => {
                setNavShow((navShow) => !navShow);
              }}
            />
          ) : (
            Nav()
          )}
          {navShow ? Nav() : null}
        </HeaderNav>
      </BoxShadow>
    </>
  );
};

export default Header;
