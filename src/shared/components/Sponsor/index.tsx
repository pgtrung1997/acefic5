import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import sponsor1 from "../../../../public/Layer 43.png";
import sponsor2 from "../../../../public/Layer 44.png";
import sponsor3 from "../../../../public/Layer 45.png";
import sponsor4 from "../../../../public/Layer 43.png";
import {
  SampleNextArrow,
  SamplePrevArrow,
  SponsorContainer,
  SponsorItem,
} from "./styles";

interface SponsorProps {
  data: any;
}

const Sponsor = ({ data }: SponsorProps) => {
  const settings = {
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    className: "sponsor",
    autoplay: true,
    speed: 1000,
    focusOnSelect: true,
    centerMode: true,
    centerPadding: "0px",
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };
  return (
    <Slider {...settings}>
      {data?.map((v: any, i: number) => {
        return (
          <SponsorContainer key={i}>
            {v.img ? (
              <SponsorItem
                src={v.img}
                layout="fill"
                objectFit="contain"
              ></SponsorItem>
            ) : (
              ""
            )}
          </SponsorContainer>
        );
      })}
    </Slider>
  );
};

export default Sponsor;
