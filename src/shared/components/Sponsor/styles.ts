import styled from "styled-components";
import theme from "../../styles/theme";
import Image from "next/image";

export const SponsorContainer = styled.div`
  background: ${theme.white};
  max-width: 150px;
  height: 90px;
  margin: auto;
  position: relative;
  cursor: pointer;
`;
export const SponsorItem = styled(Image)`
  height: 100% !important;
`;
export const SampleNextArrow = styled.div`
  visibility: hidden;
  &:before {
    font-family: none;
  }
  &::before {
    background: ${theme.green};
    border-radius: 50%;
    visibility: visible;
    padding: 7px;
    position: relative;
    right: -15px;
    box-shadow: 7px 7px 20px rgba(31, 210, 145, 0.55);
    @media ${theme.tablet} {
      display: none;
    }
  }
`;
export const SamplePrevArrow = styled.div`
  visibility: hidden;
  &:before {
    font-family: none;
  }
  &::before {
    background: ${theme.green};
    border-radius: 50%;
    visibility: visible;
    padding: 7px;
    position: relative;
    right: 25px;
    box-shadow: 7px 7px 20px rgba(31, 210, 145, 0.55);
    @media ${theme.tablet} {
      display: none;
    }
  }
`;
