import { Spin, Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ICategory } from "../../../container/HomePage/interface";
import {
  getAllProject,
  getCategory,
} from "../../../container/HomePage/redux/action";
import MyCarousel from "../MyCarousel";
import { LoadingOutlined } from "@ant-design/icons";
import theme from "../../styles/theme";

interface MyTabProps {
  showModal: () => void;
}

const { TabPane } = Tabs;

const MyTab = ({ showModal }: MyTabProps) => {
  const [windowSize, setWindowSize] = useState<any>();
  const [loading, setLoading] = useState<boolean>(true);
  const antIcon = (
    <LoadingOutlined style={{ fontSize: 50, color: theme.green }} spin />
  );
  const dispatch = useDispatch();
  const { category } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getCategory(dispatch);
    getAllProject(dispatch, "/1");
    setLoading(false);
    function handleResize() {
      setWindowSize(window.innerWidth);
    }
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  return (
    <Tabs
      tabPosition={windowSize <= 768 ? "top" : "left"}
      type="card"
      tabBarGutter={9}
      onTabClick={async (key) => {
        setLoading(true);
        await getAllProject(dispatch, `/${key}`);
        setLoading(false);
      }}
    >
      {category?.map((v: ICategory) => {
        return (
          <TabPane tab={v.tabname} key={v.id}>
            {loading ? (
              <Spin indicator={antIcon} />
            ) : (
              <MyCarousel showModal={showModal} />
            )}
          </TabPane>
        );
      })}
    </Tabs>
  );
};

export default MyTab;
