export const apiURL = {
  homepage: {
    intro: "/intro",
    about: "/about",
    market: "/market",
    projects: "/projects",
    category: "/category",
    allproject: "/allproject",
    recruitment: "/recruitment",
    news: "/news",
    partner: "/partner",
    posts: "/posts",
  },
  newspage: {
    posts: "/posts",
  },
  newsdetailpage: {
    posts: "/posts",
  },
  contactpage: {
    contact: "/contact",
  },
};
