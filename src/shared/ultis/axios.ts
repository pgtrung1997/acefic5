import axios from "axios";

const axiosApi = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
});

export const get = async (url: string) => {
  return await axiosApi.get(url).then((response) => {
    if (response.status === 200) {
      return response.data;
    }
  });
};
