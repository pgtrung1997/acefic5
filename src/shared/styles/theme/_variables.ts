export const colors = {
  blue: "#1d407d",
  green: "#1ac667",
  white: "#fff",
  black: "#000",
  gray: "#cdd3e9",
  lightGray: "#dee6f4",
  spaceCadet: "#17305c",
  slateBlue: "#5f77a3",
  water: "#eff3fa",
  royalBlue: "#2366e1",
  titanWhite: "#e9effa",
  lavender: "#ced0e5",
  mediumAquamarine: "#56d8ab",
};

export const fonts = {
  OpenSans: `'Open Sans', sans-serif`,
  Montserrat: `'Montserrat', sans-serif`,
};

export const sizes = {
  mobile: "480px",
  tablet: "768px",
  laptop: "920px",
  laptopM: "1200px",
  laptopL: "1440px",
};

export const devices = {
  mobile: `(max-width: ${sizes.mobile})`,
  tablet: `(max-width: ${sizes.tablet})`,
  laptop: `(max-width: ${sizes.laptop})`,
  laptopM: `(max-width: ${sizes.laptopM})`,
  laptopL: `(max-width: ${sizes.laptopL})`,
};
