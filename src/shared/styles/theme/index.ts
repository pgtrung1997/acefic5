import { fonts, colors, devices, sizes } from "./_variables";

const theme = {
  ...colors,
  ...fonts,
  ...devices,
};

export default theme;
