import styled, { keyframes } from "styled-components";
import theme from ".";

export interface CommonProps {
  top: string;
  right: string;
  left: string;
  bottom: string;
  margin?: string;
  weight?: string;
  size: string;
  family: string;
  color: string;
  lineHeight: string;
  styles?: string;
  flex: string;
  center: string;
  img?: string;
  rotate?: boolean;
  display?: string;
  marginlaptopL: string;
  marginlaptopM: string;
  leftlaptopM: string;
  rightlaptopM: string;
  bottomlaptopM: string;
  paddinglaptopM: string;
  widthlaptopM: string;
  marginlaptop: string;
  centerlaptop: boolean;
  widthlaptop: string;
  colorlaptop: string;
  sizelaptop: string;
  margintablet: string;
  centertablet: string;
  marginmobile: string;
  padding: string;
}

export const bounce = keyframes` 
  0%,
  100% {
    text-shadow: 0 0 7px ${theme.green};
  }
  50% {
    text-shadow: 17px 35px 40px ${theme.green};
    transform: scale(1.1);
  }
`;
export const rotate = keyframes`
  0%, 100% {
    transform: rotate(20deg);
  }
  50% {
    transform: rotate(-20deg);
  }
`;

export const WrapperSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: wrap;
  @media ${theme.laptopM} {
    align-items: center;
  }
  @media ${theme.laptop} {
    flex-direction: column;
  }
`;
export const Main = styled.div`
  /* margin-left: 391px;
  margin-right: 368px; */
  max-width: 1140px;
  margin: auto;
  @media ${theme.laptopL} {
    max-width: 1100px;
  }
  @media ${theme.laptopM} {
    max-width: 850px;
  }
  @media ${theme.laptop} {
    max-width: 700px;
  }
  @media ${theme.tablet} {
    padding: 0 30px;
  }
  /* @media ${theme.mobile} {
    padding: 0 10px;
  } */
`;
export const IndexSection = styled.div`
  position: relative;
`;

export const Section: any = styled.div<CommonProps>`
  margin: ${(props) => props.margin};
  position: relative;
  @media ${theme.laptopL} {
    margin: ${(props) => props.marginlaptopL};
  }
  @media ${theme.laptopM} {
    margin: ${(props) => props.marginlaptopM};
  }
  @media ${theme.laptop} {
    margin: ${(props) => props.marginlaptop};
    text-align: ${(props) => (props.centerlaptop ? "center" : "")};
  }
  @media ${theme.tablet} {
    margin: ${(props) => props.margintablet};
  }
  @media ${theme.mobile} {
    margin: ${(props) => props.marginmobile};
  }
`;

export const BackgroundPosition: any = styled.div<CommonProps>`
  position: absolute;
  z-index: -1;
  top: ${(props) => props.top};
  right: ${(props) => props.right};
  left: ${(props) => props.left};
  bottom: ${(props) => props.bottom};
  animation: ${(props) => (props.rotate ? rotate : null)} 10s linear infinite
    alternate;
  @media ${theme.laptopL} {
    display: ${(props) => props.display};
    max-width: 900px;
  }
  @media ${theme.laptopM} {
    max-width: 700px;
  }
  @media ${theme.laptop} {
    max-width: 100%;
  }
  @media ${theme.tablet} {
    min-width: 1000px;
  }
`;
export const IntroText = styled.h1`
  font: 700 58.33px ${theme.OpenSans};
  color: ${theme.blue};
  text-transform: uppercase;
  line-height: 1;
  @media ${theme.laptopM} {
    font-size: 45px;
  }
  @media ${theme.laptop} {
    color: ${theme.white};
    font-size: 35px;
  }
`;
export const Para: any = styled.p<CommonProps>`
  font-weight: ${(props) => props.weight};
  font-size: ${(props) => props.size};
  font-family: ${(props) => props.family};
  font-style: ${(props) => props.styles || "normal"};
  color: ${(props) => props.color};
  margin: ${(props) => props.margin};
  line-height: ${(props) => props.lineHeight || 1.5};
  @media ${theme.laptop} {
    color: ${(props) => props.colorlaptop};
    font-size: ${(props) => props.sizelaptop};
  }
  @media ${theme.tablet} {
    text-align: ${(props) => props.centertablet};
  }
`;
export const Button: any = styled.button<CommonProps>`
  position: relative;
  background: ${theme.green};
  border: none;
  padding: 9px 41px;
  margin: ${(props) => props.margin};
  cursor: pointer;
  transition: 0.3s;
  &::before,
  & > :first-child::before {
    position: absolute;
    width: 13px;
    height: 13px;
    border-color: ${theme.green};
    border-style: solid;
    content: " ";
  }
  &::before {
    top: -6px;
    left: -4px;
    border-width: 1px 0 0 1px;
  }
  & > :first-child::before {
    bottom: -6px;
    right: -6px;
    border-width: 0 1px 1px 0;
  }
  &:hover {
    background: ${theme.blue};
  }
  @media ${theme.tablet} {
    margin: ${(props) => props.margintablet};
  }
`;
export const ButtonText = styled(Para)`
  text-transform: uppercase;
  margin: 0;
`;
export const Right: any = styled.div<CommonProps>`
  margin: ${(props) => props.margin};
  flex: ${(props) => props.flex || "50%"};
  padding: ${(props) => props.padding};
  @media ${theme.laptopM} {
    margin: ${(props) => props.marginlaptopM};
    padding: ${(props) => props.paddinglaptopM};
  }
  @media ${theme.laptop} {
    margin: ${(props) => props.marginlaptop};
  }
`;
export const Left: any = styled.div<CommonProps>`
  margin: ${(props) => props.margin};
  flex: ${(props) => props.flex || "50%"};
  padding: ${(props) => props.padding};
  @media ${theme.laptopM} {
  }
`;
export const Title: any = styled.h3<CommonProps>`
  font-family: "UTMBold";
  font-size: 41.73px;
  color: ${theme.blue};
  text-transform: uppercase;
  margin: ${(props) => props.margin || 0};
  line-height: ${(props) => props.lineHeight};
  text-align: ${(props) => (props.center ? "center" : "")};
  @media ${theme.laptopM} {
    margin: ${(props) => props.marginlaptopM};
  }
  @media ${theme.tablet} {
    font-size: 35px;
    text-align: center;
    margin-bottom: 20px;
  }
`;
export const ImageContainer: any = styled.div<CommonProps>`
  margin: ${(props) => props.margin};
  text-align: ${(props) => (props.center ? "center" : "")};
  @media ${theme.laptopM} {
    margin: ${(props) => props.marginlaptopM};
  }
  @media ${theme.tablet} {
    display: none;
  }
`;
export const ImagePosition: any = styled.div<CommonProps>`
  position: relative;
  top: ${(props) => props.top};
  left: ${(props) => props.left};
  bottom: ${(props) => props.bottom};
  right: ${(props) => props.right};
  @media ${theme.laptopM} {
    max-width: ${(props) => props.widthlaptopM};
  }
  @media ${theme.mobile} {
    position: initial;
    max-width: 100%;
    margin: ${(props) => props.marginmobile};
  }
`;

export const SubHeading: any = styled.h2<CommonProps>`
  font-family: "UTMBold";
  font-size: ${(props) => props.size};
  color: ${theme.white};
  text-transform: uppercase;
  position: absolute;
  bottom: ${(props) => props.bottom};
  left: ${(props) => props.left};
  right: ${(props) => props.right};
  animation: ${bounce} 3s linear infinite;
  @media ${theme.laptopL} {
    font-size: 60px;
    margin-right: 100px;
  }
  @media ${theme.laptopM} {
    /* margin-right: 0;
    left: ${(props) => props.leftlaptopM};
    right: ${(props) => props.rightlaptopM};
    bottom: ${(props) => props.bottomlaptopM}; */
    display: none;
  }
`;
export const SecondButton = styled.button`
  background: ${theme.green};
  border: none;
  border-radius: 50px;
  padding: 20px 46px;
  margin: 6px 0 0 12px;
  box-shadow: 15px 15px 35px rgba(31, 211, 146, 0.45);
  cursor: pointer;
  transition: 0.3s;
  &:hover {
    background: ${theme.blue};
    box-shadow: 15px 15px 35px rgba(0, 0, 0, 0.45);
  }
`;
export const AlignCenter = styled.div`
  text-align: center;
`;
export const PartnerTitle = styled.h4`
  font: 700 20.83px ${theme.Montserrat};
  text-transform: uppercase;
  text-align: center;
  margin: 33px 0;
`;
export const PartnerWrapper = styled.div`
  background: linear-gradient(
    342deg,
    rgba(42, 241, 158, 1) 0%,
    rgba(255, 255, 255, 1) 21%,
    rgba(255, 255, 255, 1) 86%,
    rgba(42, 241, 158, 1) 100%
  );
  border-radius: 10px;
  max-width: 1005px;
  height: 216px;
  padding: 3px;
  margin: 0 auto;
  position: relative;
  top: 98px;
  z-index: 1;
  @media ${theme.laptopM} {
    max-width: 700px;
  }
  @media ${theme.tablet} {
    position: initial;
    max-width: 100%;
    margin-top: 50px;
  }
`;
export const PartnerContainer: any = styled.div<CommonProps>`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
`;

export const NewsPara: any = styled.p<CommonProps>`
  font-family: ${(props) => props.family};
  font-size: ${(props) => props.size};
  color: ${(props) => props.color};
  margin: ${(props) => props.margin || "0"};
  font-weight: ${(props) => props.weight};
  text-align: start;
  line-height: ${(props) => props.lineHeight};
  text-align: justify;
  @media ${theme.laptop} {
    font-size: ${(props) => props.sizelaptop};
  }
`;

export const Margin: any = styled.div<CommonProps>`
  margin: ${(props) => props.margin};
`;
