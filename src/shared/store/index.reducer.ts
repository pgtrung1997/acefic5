import { ContactpageReducer } from "../../container/ContactPage/redux/reducer";
import { NewsDetailpageReducer } from "../../container/NewsDetailPage/redux/reducer";
import { NewspageReducer } from "../../container/NewsPage/redux/reducer";
import { combineReducers } from "redux";
import { HomepageReducer } from "../../container/HomePage/redux/reducer";

export const rootReducer = combineReducers({
  HomepageData: HomepageReducer,
  NewspageData: NewspageReducer,
  NewsDetailData: NewsDetailpageReducer,
  ContactData: ContactpageReducer,
});
