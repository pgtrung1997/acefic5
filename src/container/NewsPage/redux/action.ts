import { apiURL } from "./../../../shared/ultis/url";
import { get } from "./../../../shared/ultis/axios";
import * as Types from "./action-type";

export const getPost = async (dispatch: any) => {
  try {
    const res = await get(apiURL.newspage.posts);
    if (res) {
      dispatch({ type: Types.RECEIVE_POST, payload: res });
    }
  } catch {}
};
