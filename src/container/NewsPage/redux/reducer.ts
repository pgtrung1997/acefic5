import * as Types from "./action-type";

const initialState = {
  posts: [],
};

export const NewspageReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case Types.RECEIVE_POST:
      const posts = action.payload;
      return { ...state, posts };
    default:
      return state;
  }
};
