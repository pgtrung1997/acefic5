import React from "react";
import styled from "styled-components";
import Breadcrumb from "../../shared/components/Breadcrumb";
import Footer from "../../shared/components/Footer";
import Header from "../../shared/components/Header";
import {
  Left,
  Main,
  Margin,
  WrapperSection,
} from "../../shared/styles/theme/common";

import NewsCategory from "../../shared/components/NewsCategory";
import theme from "../../shared/styles/theme";
import MainSection from "./MainSection";

const WrapperNews = styled(WrapperSection)`
  @media ${theme.laptopM} {
    flex-direction: column;
  }
`;
const NewsLeft = styled(Left)`
  @media ${theme.laptopM} {
    margin-top: 50px;
  }
`;

const NewsPage = () => {
  return (
    <>
      <Header sidepage />
      <Breadcrumb routing="Tin tức" />
      <Main>
        <Margin margin="65px 0">
          <WrapperNews>
            <MainSection />
            <NewsLeft flex="20%">
              <NewsCategory />
            </NewsLeft>
          </WrapperNews>
        </Margin>
      </Main>
      <Footer sidepage />
    </>
  );
};

export default NewsPage;
