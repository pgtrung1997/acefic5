import Link from "next/link";
import React, { useEffect } from "react";
import Navigation from "../../../shared/components/Navigation";
import NewsHeader from "../../../shared/components/NewsHeader";
import theme from "../../../shared/styles/theme";
import { Margin, NewsPara, Right } from "../../../shared/styles/theme/common";
import {
  NewsRight,
  NewsItem,
  MyImage,
  Date,
  DateUploadContainer,
  Month,
} from "./styles";
import Image from "next/image";
import { getPost } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";
import { IPost } from "../interface";

const MainSection = () => {
  const dispatch = useDispatch();
  const { posts } = useSelector((state: any) => state.NewspageData);
  useEffect(() => {
    getPost(dispatch);
  }, []);

  return (
    <Right flex="69%">
      <NewsRight>
        {posts?.map((v: IPost) => {
          return (
            <NewsItem key={v.id}>
              <DateUploadContainer>
                <Date>25</Date>
                <Month>January</Month>
              </DateUploadContainer>
              <Link href={`/newsdetail/${encodeURIComponent(v.id)}`}>
                <div>
                  <MyImage>
                    {v.imgsm ? (
                      <Image
                        src={v.imgsm}
                        layout="responsive"
                        width={403}
                        height={291}
                      ></Image>
                    ) : (
                      ""
                    )}
                  </MyImage>
                  <Margin margin="22px 0 14px">
                    <NewsHeader upload={v.date} popular={v.view} start={true} />
                  </Margin>
                  <NewsPara
                    family={theme.OpenSans}
                    size="20.83px"
                    weight="600"
                    color={theme.royalBlue}
                    lineHeight="1.15"
                  >
                    {v.title}
                  </NewsPara>
                  <NewsPara
                    family={theme.OpenSans}
                    size="14.58px"
                    color={theme.blue}
                    margin="15px 0 0"
                    lineHeight="1.65"
                  >
                    {v.content[0]}
                  </NewsPara>
                </div>
              </Link>
            </NewsItem>
          );
        })}
      </NewsRight>
      <Navigation />
    </Right>
  );
};

export default MainSection;
