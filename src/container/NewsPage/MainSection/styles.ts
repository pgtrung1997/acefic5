import styled from "styled-components";
import theme from "../../../shared/styles/theme";

export const NewsRight = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  @media ${theme.laptop} {
    justify-content: center;
  }
`;
export const MyImage = styled.div`
  border-radius: 10px;
  transition: 0.3s;
`;
export const NewsItem = styled.div`
  max-width: 403px;
  margin-bottom: 58px;
  position: relative;
  cursor: pointer;
  :hover {
    ${MyImage} {
      box-shadow: 10px 10px 45px rgba(48, 78, 142, 0.3);
    }
  }
`;
export const DateUploadContainer = styled.div`
  position: absolute;
  top: 20px;
  left: 18px;
  background: ${theme.blue};
  border-radius: 5px;
  z-index: 1;
  padding: 11px 5px;
  text-align: center;
`;
export const Date = styled.h6`
  font: 700 20.81px ${theme.OpenSans};
  color: ${theme.white};
  margin: -5px;
`;
export const Month = styled.p`
  font: 500 12.49px ${theme.OpenSans};
  color: ${theme.white};
  margin: 0;
`;
