export interface IPost {
  id: number;
  title: string;
  date: string;
  view: number;
  imgsm: string;
  content: string[];
}
