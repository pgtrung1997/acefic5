import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getIntro } from "../redux/action";
import theme from "../../../shared/styles/theme";
import {
  IntroText,
  Para,
  Section,
  Button,
  ButtonText,
} from "../../../shared/styles/theme/common";

const SectionIntro = () => {
  const dispatch = useDispatch();
  const { intro } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getIntro(dispatch);
  }, []);
  return (
    <Section
      margin="224px 600px 0 0"
      marginlaptopL="150px 600px 0 0"
      marginlaptopM="100px 450px 0 0"
      marginlaptop="200px 100px 0"
      margintablet="190px 0 0"
      centerlaptop
      data-aos="fade-up"
    >
      <IntroText>{intro.title}</IntroText>
      <Para
        family={theme.OpenSans}
        size="25px"
        color={theme.blue}
        margin="35px 0 -2px"
        lineHeight="1.4"
        colorlaptop={theme.white}
        sizelaptop="18px"
      >
        {intro.content}
      </Para>
      <Button margin="40px 0 0 5px">
        <ButtonText family={theme.OpenSans} size="14.58px" color={theme.white}>
          Xem thêm
        </ButtonText>
      </Button>
    </Section>
  );
};

export default SectionIntro;
