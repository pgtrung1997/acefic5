import { IndexSection, Main } from "../../shared/styles/theme/common";
import Footer from "../../shared/components/Footer";
import { useState } from "react";
import SectionAbout from "./SectionAbout";
import SectionMarket from "./SectionMarket";
import SectionIntro from "./SectionIntro";
import SectionRecruitment from "./SectionRecruitment";
import SectionProject from "./SectionProject";
import SectionNews from "./SectionNews";
import SectionPartner from "./SectionPartner";
import SectionBackground from "./SectionBackground";
import Header from "../../shared/components/Header";

const HomePage = () => {
  return (
    <IndexSection>
      <Header />
      <Main>
        <SectionIntro />
        <SectionAbout />
        <SectionMarket />
        <SectionProject />
        <SectionRecruitment />
        <SectionNews />
        <SectionPartner />
      </Main>
      <SectionBackground />
      <Footer />
    </IndexSection>
  );
};

export default HomePage;
