import React, { useEffect } from "react";
import Sponsor from "../../../shared/components/Sponsor";
import {
  PartnerContainer,
  PartnerTitle,
  PartnerWrapper,
  Section,
} from "../../../shared/styles/theme/common";
import Image from "next/image";
import background from "../../../../public/Layer48.jpg";
import { getPartner } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";

const SectionPartner = () => {
  const dispatch = useDispatch();
  const { partner } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getPartner(dispatch);
  }, []);
  return (
    <Section>
      <PartnerWrapper data-aos="zoom-in">
        <div
          style={{
            borderRadius: "10px",
            overflow: "hidden",
            maxHeight: "100%",
          }}
        >
          <Image src={background} layout="fixed"></Image>
        </div>
        <PartnerContainer>
          <PartnerTitle>{partner.title}</PartnerTitle>
          <Sponsor data={partner.logo} />
        </PartnerContainer>
      </PartnerWrapper>
    </Section>
  );
};

export default SectionPartner;
