import React, { useEffect, useState } from "react";
import MyModal from "../../../shared/components/MyModal";
import MyTab from "../../../shared/components/MyTab";
import Portal from "../../../shared/components/Portal";
import {
  Section,
  ButtonText,
  Title,
  ImageContainer,
  SubHeading,
} from "../../../shared/styles/theme/common";
import Image from "next/image";
import theme from "../../../shared/styles/theme";
import Divide1 from "../../../../public/Rectangle248.png";
import { getProject } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";

const SectionProject = () => {
  const dispatch = useDispatch();
  const { projects } = useSelector((state: any) => state.HomepageData);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  useEffect(() => {
    getProject(dispatch);
  }, []);
  return (
    <Section
      margin="331px 0 0"
      marginlaptopM="250px 0 0"
      marginmobile="150px 0 0"
      data-aos="fade-up"
    >
      <ButtonText
        family={theme.OpenSans}
        size="20.83px"
        color={theme.green}
        centertablet="center"
      >
        {projects.subtitle}
      </ButtonText>
      <Title margin="0 950px 0 0" lineHeight="1.4" marginlaptopM="0">
        {projects.title}
      </Title>
      <ImageContainer margin="22px 0 19px -2px">
        <Image src={Divide1}></Image>
      </ImageContainer>
      <MyTab showModal={showModal} />
      <Portal selector="#myportal">
        <MyModal isModalVisible={isModalVisible} handleCancel={handleCancel} />
      </Portal>
      <SubHeading
        size="87.5px"
        bottom="-24px"
        left="-6px"
        bottomlaptopM="-160px"
      >
        Projects
      </SubHeading>
    </Section>
  );
};

export default SectionProject;
