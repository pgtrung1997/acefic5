export interface IIntro {
  title: string;
  content: string;
}

export interface IAbout {
  title: string;
  desc: string;
  subheading: string;
  img: string;
}

export interface IField {
  id: string;
  title: string;
  desc: string;
}

export interface IMarket {
  title: string;
  field: IField[];
  subheading: string;
  img: string;
}

export interface IProject {
  id: number;
  img: string;
}

export interface ICategory {
  id: number;
  tabname: string;
  project: IProject[];
}

export interface IProjects {
  subtitle: string;
  title: string;
  subheading: string;
  category: ICategory[];
}

export interface News {
  title: string;
  subheading: string;
}

export interface IPartner {
  title: string;
  logo: IProject[];
}

export interface IPost {
  id: number;
  title: string;
  date: string;
  view: number;
  imgsm: string;
  content: string[];
}
