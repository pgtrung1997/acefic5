import React, { useEffect } from "react";
import {
  Button,
  ButtonText,
  ImageContainer,
  Left,
  Para,
  Right,
  Section,
  SubHeading,
  Title,
  WrapperSection,
} from "../../../shared/styles/theme/common";
import Image from "next/image";
import Divide1 from "../../../../public/Rectangle248.png";
import theme from "../../../shared/styles/theme";
import { getAbout } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";

const SectionAbout = () => {
  const dispatch = useDispatch();
  const { about } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getAbout(dispatch);
  }, []);

  return (
    <Section
      margin="408px 0 0"
      marginlaptopL="200px 0 0"
      marginlaptop="300px 0 0"
      margintablet="400px 0 0"
      marginmobile="250px 0 0"
      data-aos="fade-up"
    >
      <WrapperSection>
        <Left>
          {about.img ? (
            <Image src={about.img} width={579} height={444}></Image>
          ) : (
            ""
          )}
        </Left>
        <Right margin="40px 0 0 60px" flex="40%" marginlaptopM="0">
          <Title margin="0 0 0 -10px">{about.title}</Title>
          <ImageContainer margin="17px 0 37px 10px" marginlaptopM="10px 0">
            <Image src={Divide1}></Image>
          </ImageContainer>
          <Para
            family={theme.OpenSans}
            size="14.57px"
            color={theme.blue}
            styles="italic"
            lineHeight="2"
          >
            {about.desc}
          </Para>
          <Button margin="20px 0 0 5px">
            <ButtonText
              family={theme.OpenSans}
              size="14.58px"
              color={theme.white}
            >
              Xem thêm
            </ButtonText>
          </Button>
        </Right>
      </WrapperSection>
      <SubHeading
        size="90.08px"
        bottom="-130px"
        left="636px"
        leftlaptopM="400px"
      >
        {about.subheading}
      </SubHeading>
    </Section>
  );
};

export default SectionAbout;
