import React from "react";
import { BackgroundPosition } from "../../../shared/styles/theme/common";
import Image from "next/image";
import headerBackground from "../../../../public/RoundedRectangle.png";
import background1 from "../../../../public/Layer 24.png";
import background2 from "../../../../public/Layer 11 copy.png";
import background3 from "../../../../public/business-home-rev-slider-img-03.png";
import background4 from "../../../../public/business-home-rev-slider-img-04.png";
import background5 from "../../../../public/Layer 31.png";
import background6 from "../../../../public/business-home-rev-slider-img-07.png";
import background7 from "../../../../public/Layer 33.png";

const SectionBackground = () => {
  return (
    <>
      <BackgroundPosition top="0" right="0" data-aos="fade-down">
        <Image src={headerBackground}></Image>
      </BackgroundPosition>
      <BackgroundPosition top="621px" left="0" display="none">
        <Image src={background1}></Image>
      </BackgroundPosition>
      <BackgroundPosition top="1362px" right="0" display="none">
        <Image src={background2}></Image>
      </BackgroundPosition>
      <BackgroundPosition top="889px" left="680px" rotate={true} display="none">
        <Image src={background3}></Image>
      </BackgroundPosition>
      <BackgroundPosition
        top="1257px"
        left="226px"
        rotate={true}
        display="none"
      >
        <Image src={background4}></Image>
      </BackgroundPosition>
      <BackgroundPosition
        top="1556px"
        left="-751px"
        rotate={true}
        display="none"
      >
        <Image src={background5}></Image>
      </BackgroundPosition>
      <BackgroundPosition
        top="3161px"
        right="100px"
        rotate={true}
        display="none"
      >
        <Image src={background6}></Image>
      </BackgroundPosition>
      <BackgroundPosition
        bottom="981px"
        right="396px"
        rotate={true}
        display="none"
      >
        <Image src={background7}></Image>
      </BackgroundPosition>
    </>
  );
};

export default SectionBackground;
