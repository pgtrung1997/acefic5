import React, { useEffect } from "react";
import {
  Section,
  Left,
  WrapperSection,
  Right,
  SubHeading,
  ImagePosition,
  ImageContainer,
  Title,
} from "../../../shared/styles/theme/common";
import Image from "next/image";
import Category from "../../../shared/components/Category";
import Divide1 from "../../../../public/Rectangle248.png";
import { getMarket } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";
import { IField } from "../interface";

const SectionMarket = () => {
  const dispatch = useDispatch();
  const { market } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getMarket(dispatch);
  }, []);

  return (
    <Section
      margin="255px 0 0"
      marginlaptopM="200px 0 0"
      marginlaptop="150px 0 0"
      data-aos="fade-down"
    >
      <WrapperSection>
        <Left flex="40%">
          <Title>{market.title}</Title>
          <ImageContainer margin="14px 0 12px 10px">
            <Image src={Divide1}></Image>
          </ImageContainer>
          {market.field?.map((v: IField, i: number) => {
            return <Category index={v.id} key={i} title={v.title} />;
          })}
        </Left>
        <Right flex="50%">
          <ImagePosition
            top="137px"
            right="-38px"
            widthlaptopM="90%"
            marginmobile="100px 0 0"
          >
            {market.img ? (
              <Image src={market.img} width={607} height={426}></Image>
            ) : (
              ""
            )}
          </ImagePosition>
        </Right>
      </WrapperSection>
      <SubHeading size="83.33px" bottom="-213px" left="234px">
        {market.subheading}
      </SubHeading>
    </Section>
  );
};

export default SectionMarket;
