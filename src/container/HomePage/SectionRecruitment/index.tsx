import React, { useEffect } from "react";
import theme from "../../../shared/styles/theme";
import {
  Section,
  WrapperSection,
  Left,
  ImagePosition,
  ImageContainer,
  Right,
  Para,
  SecondButton,
  ButtonText,
  Title,
  SubHeading,
} from "../../../shared/styles/theme/common";
import Image from "next/image";
import Divide1 from "../../../../public/Rectangle248.png";
import { getRecruit } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";

const SectionRecruitment = () => {
  const dispatch = useDispatch();
  const { recruitment } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getRecruit(dispatch);
  }, []);
  return (
    <Section
      margin="212px 0 0"
      marginlaptopM="250px 0 0"
      margintablet="150px 0 0"
      data-aos="fade-down"
    >
      <WrapperSection>
        <Left flex="50%">
          <ImagePosition top="-11px" left="16px">
            {recruitment.img ? (
              <Image src={recruitment.img} width={457} height={590}></Image>
            ) : (
              ""
            )}
          </ImagePosition>
        </Left>
        <Right flex="40%" margin="97px 0 0 51px" marginlaptop="50px 0 0">
          <Title>{recruitment.title}</Title>
          <ImageContainer margin="20px 0 31px 10px">
            <Image src={Divide1}></Image>
          </ImageContainer>
          <Para
            family={theme.OpenSans}
            size="14.57px"
            color={theme.blue}
            lineHeight="1.7"
            margin="0 0 20px 11px"
          >
            {recruitment.desc}
          </Para>
          <SecondButton>
            <ButtonText
              family={theme.OpenSans}
              size="14.58px"
              color={theme.white}
            >
              Nộp đơn {">"}
              {">"}
            </ButtonText>
          </SecondButton>
        </Right>
      </WrapperSection>
      <SubHeading
        size="83.33px"
        bottom="9px"
        left="484px"
        bottomlaptopM="-50px"
      >
        {recruitment.subheading}
      </SubHeading>
    </Section>
  );
};

export default SectionRecruitment;
