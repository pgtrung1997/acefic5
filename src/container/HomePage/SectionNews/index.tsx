import React, { useEffect } from "react";
import News from "../../../shared/components/News";
import theme from "../../../shared/styles/theme";
import {
  Section,
  Title,
  ImageContainer,
  Button,
  ButtonText,
  SubHeading,
  AlignCenter,
} from "../../../shared/styles/theme/common";
import Image from "next/image";
import Divide2 from "../../../../public/Rectangle248copy.png";
import { useDispatch, useSelector } from "react-redux";
import { getNew } from "../redux/action";

const SectionNews = () => {
  const dispatch = useDispatch();
  const { news } = useSelector((state: any) => state.HomepageData);
  useEffect(() => {
    getNew(dispatch);
  }, []);
  return (
    <Section margin="179px 0 0" marginlaptopM="150px 0 0" data-aos="fade-up">
      <Title center>{news[0]?.title}</Title>
      <ImageContainer margin="16px 0 89px" center>
        <Image src={Divide2}></Image>
      </ImageContainer>
      <News data={news[1]} />
      <AlignCenter>
        <Button margin="166px 0 0" margintablet="50px 0 0">
          <ButtonText
            family={theme.OpenSans}
            size="14.58px"
            color={theme.white}
          >
            Xem tiếp
          </ButtonText>
        </Button>
      </AlignCenter>
      <SubHeading
        size="83.33px"
        bottom="-71px"
        right="-102px"
        rightlaptopM="600px"
      >
        {news[0]?.subheading}
      </SubHeading>
    </Section>
  );
};

export default SectionNews;
