import * as Types from "./action-type";

const initialState = {
  intro: [],
  about: [],
  market: [],
  projects: [],
  category: [],
  recruitment: [],
  news: [],
  partner: [],
  projectList: [],
};

export const HomepageReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case Types.RECEIVE_INTRO:
      const intro = action.payload;
      return { ...state, intro };
    case Types.RECEIVE_ABOUT:
      const about = action.payload;
      return { ...state, about };
    case Types.RECEIVE_MARKET:
      const market = action.payload;
      return { ...state, market };
    case Types.RECEIVE_PROJECT:
      const projects = action.payload;
      return { ...state, projects };
    case Types.RECEIVE_ALLPROJECT:
      const projectList = action.payload;
      return { ...state, projectList };
    case Types.RECEIVE_RECRUITMENT:
      const recruitment = action.payload;
      return { ...state, recruitment };
    case Types.RECEIVE_CATEGORY:
      const category = action.payload;
      return { ...state, category };
    case Types.RECEIVE_NEWS:
      const news = action.payload;
      return { ...state, news };
    case Types.RECEIVE_PARTNER:
      const partner = action.payload;
      return { ...state, partner };
    default:
      return state;
  }
};
