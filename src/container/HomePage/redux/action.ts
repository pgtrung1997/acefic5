import { get } from "../../../shared/ultis/axios";
import { apiURL } from "./../../../shared/ultis/url";
import * as Types from "./action-type";

export const getIntro = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.intro);
    if (res) {
      dispatch({ type: Types.RECEIVE_INTRO, payload: res });
    }
  } catch {}
};

export const getAbout = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.about);
    if (res) {
      dispatch({ type: Types.RECEIVE_ABOUT, payload: res });
    }
  } catch {}
};

export const getMarket = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.market);
    if (res) {
      dispatch({ type: Types.RECEIVE_MARKET, payload: res });
    }
  } catch {}
};

export const getProject = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.projects);
    if (res) {
      dispatch({ type: Types.RECEIVE_PROJECT, payload: res });
    }
  } catch {}
};
export const getAllProject = async (dispatch: any, url: string) => {
  try {
    const res = await get(apiURL.homepage.allproject + url);
    if (res) {
      dispatch({ type: Types.RECEIVE_ALLPROJECT, payload: res });
    }
  } catch {}
};
export const getCategory = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.category);
    if (res) {
      dispatch({ type: Types.RECEIVE_CATEGORY, payload: res });
    }
  } catch {}
};

export const getRecruit = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.recruitment);
    if (res) {
      dispatch({ type: Types.RECEIVE_RECRUITMENT, payload: res });
    }
  } catch {}
};

export const getNew = (dispatch: any) => {
  const endpoints = [apiURL.homepage.news, apiURL.homepage.posts];
  Promise.all(endpoints.map((endpoint) => get(endpoint))).then((res) => {
    dispatch({
      type: Types.RECEIVE_NEWS,
      payload: [res[0], res[1]],
    });
  });
};

export const getPartner = async (dispatch: any) => {
  try {
    const res = await get(apiURL.homepage.partner);
    if (res) {
      dispatch({ type: Types.RECEIVE_PARTNER, payload: res });
    }
  } catch {}
};
