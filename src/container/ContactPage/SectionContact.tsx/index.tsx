import React, { useEffect } from "react";
import theme from "../../../shared/styles/theme";
import { Main } from "../../../shared/styles/theme/common";
import {
  ContactList,
  WrapperContact,
  Icon,
  IconWhite,
  IconContainer,
  ContactPara,
} from "./styles";
import Image from "next/image";
import location from "../../../../public/Layer 25.png";
import locationWhite from "../../../../public/Layer 25-white.png";
import phone from "../../../../public/Layer 26.png";
import phoneWhite from "../../../../public/Layer 26-white.png";
import envelop from "../../../../public/Shape 1.png";
import envelopWhite from "../../../../public/Shape 1-white.png";
import { getContact } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";

const SectionContact = () => {
  const dispatch = useDispatch();
  const { contact } = useSelector((state: any) => state.ContactData);
  useEffect(() => {
    getContact(dispatch);
  }, []);
  return (
    <Main>
      <WrapperContact>
        <ContactList flex="30%">
          <IconContainer>
            <Icon>
              <Image src={location}></Image>
            </Icon>
            <IconWhite>
              <Image src={locationWhite}></Image>
            </IconWhite>
          </IconContainer>
          <ContactPara
            family={theme.OpenSans}
            size="14.58px"
            margin="0 0 0 10px"
          >
            {contact.location}
          </ContactPara>
        </ContactList>
        <ContactList flex="30%">
          <IconContainer>
            <Icon>
              <Image src={phone}></Image>
            </Icon>
            <IconWhite>
              <Image src={phoneWhite}></Image>
            </IconWhite>
          </IconContainer>
          <ContactPara
            family={theme.OpenSans}
            size="24.97px"
            margin="0 0 0 16px"
          >
            {contact.phone}
          </ContactPara>
        </ContactList>
        <ContactList flex="30%">
          <IconContainer>
            <Icon>
              <Image src={envelop}></Image>
            </Icon>
            <IconWhite>
              <Image src={envelopWhite}></Image>
            </IconWhite>
          </IconContainer>
          <ContactPara
            family={theme.OpenSans}
            size="16.65px"
            margin="0 0 0 30px"
          >
            {contact.email}
          </ContactPara>
        </ContactList>
      </WrapperContact>
    </Main>
  );
};

export default SectionContact;
