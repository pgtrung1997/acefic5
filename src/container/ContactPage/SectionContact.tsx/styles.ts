import styled from "styled-components";
import theme from "../../../shared/styles/theme";
import {
  Para,
  Right,
  WrapperSection,
} from "../../../shared/styles/theme/common";

export const WrapperContact = styled(WrapperSection)`
  @media ${theme.laptop} {
    align-items: flex-start;
  }
`;
export const Icon = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
export const IconWhite = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: none;
`;
export const IconContainer = styled.div`
  position: relative;
  background: ${theme.water};
  padding: 47px;
  border-radius: 50%;
  transition: 0.3s;
`;

export const ContactList = styled(Right)`
  display: flex;
  align-items: center;
  margin: 60px 7px 0;
  cursor: pointer;
  :hover {
    ${IconContainer} {
      background: ${theme.green};
      box-shadow: 5px 5px 25px rgba(27, 205, 141, 0.55);
    }
    ${Icon} {
      display: none;
    }
    ${IconWhite} {
      display: block;
    }
  }
  ${theme.laptop} {
    align-self: flex-start;
  }
`;

export const ContactPara = styled(Para)`
  word-break: break-all;
  margin-left: 10px;
`;
