export interface IContact {
  location: string;
  phone: string;
  email: string;
}
