import styled from "styled-components";
import theme from "../../../shared/styles/theme";
import { Main } from "../../../shared/styles/theme/common";

interface ContactProps {
  margin?: string;
  width: string;
  background: string;
}

export const MapContainer = styled.div`
  position: relative;
  margin: 60px 0 -74px;
  @media ${theme.laptop} {
    margin-bottom: -68px;
  }
`;
export const Map = styled.iframe`
  width: 70%;
  @media ${theme.laptop} {
    width: 100%;
  }
`;
export const Form = styled.form`
  margin-top: 26px;
  text-align: justify;
  @media ${theme.laptop} {
    text-align: center;
  }
`;
export const ContactTitle = styled.h3`
  font-family: ${theme.OpenSans};
  font-size: 25px;
  font-weight: 700;
  color: ${theme.white};
  text-transform: uppercase;
  margin: 29px 0 2px;
  @media ${theme.laptop} {
    margin-top: 0;
  }
`;
export const ContactContainer = styled.div`
  background: ${theme.green};
  position: absolute;
  top: 0;
  bottom: 6px;
  right: 0;
  left: 0;
  clip-path: polygon(54% 0, 100% 0, 100% 100%, 45% 100%);
  @media ${theme.laptopM} {
    left: -100px;
  }
  @media ${theme.laptop} {
    position: initial;
    clip-path: none;
    padding: 30px 0;
    margin-top: -6px;
  }
`;
export const FormWrapper = styled.div`
  max-width: 457px;
  margin-left: auto;
  @media ${theme.laptop} {
    max-width: 100%;
    text-align: center;
  }
`;
export const FormContainer = styled(Main)`
  @media ${theme.laptopM} {
    max-width: 1300px;
  }
`;
export const Input: any = styled.input<ContactProps>`
  border: none;
  border-radius: 2px;
  background: ${(props) => props.background};
  width: ${(props) => props.width};
  padding: 9px 17px;
  margin: ${(props) => props.margin};
  ::placeholder {
    font-family: ${theme.OpenSans};
    font-size: 14.58px;
    color: ${theme.white};
  }
  @media ${theme.laptopM} {
    width: calc(40%);
  }
  @media ${theme.mobile} {
    width: 100%;
    margin: 0 0 10px 0;
  }
`;
export const TextArea: any = styled.textarea`
  width: 100%;
  margin-top: 10px;
  background: ${theme.mediumAquamarine};
  border: none;
  border-radius: 2px;
  padding: 11px 17px;
  ::placeholder {
    font-family: ${theme.OpenSans};
    font-size: 14.58px;
    color: ${theme.white};
  }
  @media ${theme.laptopM} {
    width: calc(80% + 10px);
  }
  @media ${theme.mobile} {
    width: 100%;
    margin: 0;
  }
`;
export const Submit = styled.input`
  width: 100%;
  border: none;
  border-radius: 2px;
  font: 500 14.58px ${theme.OpenSans};
  background: ${theme.white};
  padding: 10px 0;
  @media ${theme.laptopM} {
    width: calc(80% + 10px);
  }
  @media ${theme.mobile} {
    width: 100%;
  }
`;
