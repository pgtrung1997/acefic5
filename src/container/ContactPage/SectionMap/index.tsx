import React from "react";
import theme from "../../../shared/styles/theme";
import { Para } from "../../../shared/styles/theme/common";
import {
  MapContainer,
  Map,
  ContactContainer,
  FormContainer,
  FormWrapper,
  ContactTitle,
  Form,
  Input,
  TextArea,
  Submit,
} from "./styles";

const SectionMap = () => {
  return (
    <MapContainer>
      <Map
        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14892.837348971!2d105.801541!3d21.0642997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5275e9c84f83958e!2sACEFIC!5e0!3m2!1svi!2s!4v1646102948568!5m2!1svi!2s"
        width="600"
        height="452"
        style={{ border: "0" }}
        loading="lazy"
      ></Map>
      <ContactContainer>
        <FormContainer>
          <FormWrapper>
            <ContactTitle>Liên hệ</ContactTitle>
            <Para family={theme.OpenSans} size="14.58px" color={theme.white}>
              Cung cấp thông tin để chúng tôi có thể hỗ trợ bạn
            </Para>
            <Form>
              <Input
                type="text"
                name="name"
                placeholder="Họ và tên"
                margin="0 10px 10px 0"
                width="calc(50% - 5px)"
                background={theme.mediumAquamarine}
              />
              <Input
                type="text"
                name="organize"
                placeholder="Tổ chức"
                width="calc(50% - 5px)"
                background={theme.mediumAquamarine}
              />
              <Input
                type="text"
                name="email  "
                placeholder="Email"
                margin="0 10px 0 0"
                width="calc(50% - 5px)"
                background={theme.mediumAquamarine}
              />
              <Input
                type="text"
                name="phone"
                placeholder="Số điện thoại"
                width="calc(50% - 5px)"
                background={theme.mediumAquamarine}
              />
              <TextArea rows="5" placeholder="Nội dung..." />
              <Submit type="submit" value="GỬI TIN" />
            </Form>
          </FormWrapper>
        </FormContainer>
      </ContactContainer>
    </MapContainer>
  );
};

export default SectionMap;
