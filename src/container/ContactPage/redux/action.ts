import { apiURL } from "./../../../shared/ultis/url";
import { get } from "../../../shared/ultis/axios";
import * as Types from "./action-type";

export const getContact = async (dispatch: any) => {
  try {
    const res = await get(apiURL.contactpage.contact);
    if (res) {
      dispatch({ type: Types.RECEIVE_CONTACT, payload: res });
    }
  } catch {}
};
