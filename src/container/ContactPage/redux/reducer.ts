import * as Types from "./action-type";

const initialState = {
  contact: [],
};

export const ContactpageReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case Types.RECEIVE_CONTACT:
      const contact = action.payload;
      return { ...state, contact };
    default:
      return state;
  }
};
