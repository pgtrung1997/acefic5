import React from "react";
import Breadcrumb from "../../shared/components/Breadcrumb";
import Footer from "../../shared/components/Footer";
import Header from "../../shared/components/Header";
import SectionContact from "./SectionContact.tsx";
import SectionMap from "./SectionMap";

const ContactPage = () => {
  return (
    <>
      <Header sidepage />
      <Breadcrumb routing="Liên hệ" />
      <SectionContact />
      <SectionMap />
      <Footer sidepage />
    </>
  );
};

export default ContactPage;
