import React, { useEffect } from "react";
import styled from "styled-components";
import Breadcrumb from "../../shared/components/Breadcrumb";
import Footer from "../../shared/components/Footer";
import Header from "../../shared/components/Header";
import NewsCategory from "../../shared/components/NewsCategory";
import {
  Left,
  Main,
  Margin,
  NewsPara,
  WrapperSection,
} from "../../shared/styles/theme/common";
import theme from "../../shared/styles/theme";
import MainSection from "./MainSection";
import { getNewsPost } from "./redux/action";
import { useDispatch, useSelector } from "react-redux";

interface NewsDetailPageProps {
  postId: any;
}

const WrapperNews = styled(WrapperSection)`
  @media ${theme.laptopM} {
    flex-direction: column;
  }
`;
const NewsLeft = styled(Left)`
  @media ${theme.laptopM} {
    margin-top: 50px;
  }
`;

const NewsDetailPage = ({ postId }: NewsDetailPageProps) => {
  const dispatch = useDispatch();
  const { post } = useSelector((state: any) => state.NewsDetailData);
  useEffect(() => {
    getNewsPost(dispatch, postId);
  }, []);
  return (
    <>
      <Header sidepage />
      <Breadcrumb routing="Tin tức" />
      <Main>
        <Margin margin="65px 0">
          <WrapperNews>
            <MainSection post={post} />
            <NewsLeft flex="20%">
              <NewsCategory />
            </NewsLeft>
          </WrapperNews>
        </Margin>
      </Main>
      <Footer sidepage />
    </>
  );
};

export default NewsDetailPage;
