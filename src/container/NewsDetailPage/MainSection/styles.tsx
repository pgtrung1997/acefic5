import styled from "styled-components";
import theme from "../../../shared/styles/theme";
import {
  Left,
  NewsPara,
  WrapperSection,
} from "../../../shared/styles/theme/common";

export const DetailContainer = styled.div`
  border: 1px solid ${theme.gray};
  padding: 28px 39px 86px 32px;
`;
export const WrapperNews = styled(WrapperSection)`
  @media ${theme.laptopM} {
    flex-direction: column;
  }
`;
export const DetailPara = styled(NewsPara)`
  white-space: pre-wrap;
  text-align: justify;
`;
export const NewsLeft = styled(Left)`
  @media ${theme.laptopM} {
    margin-top: 50px;
  }
`;
