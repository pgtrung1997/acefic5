import React from "react";
import NewsHeader from "../../../shared/components/NewsHeader";
import theme from "../../../shared/styles/theme";
import { Margin, NewsPara, Right } from "../../../shared/styles/theme/common";
import { DetailContainer, DetailPara } from "./styles";
import Image from "next/image";
import { IPost } from "../interface";

interface MainSectionProps {
  post: IPost;
}

const MainSection = ({ post }: MainSectionProps) => {
  return (
    <Right flex="69%">
      <DetailContainer>
        <NewsPara
          family={theme.OpenSans}
          weight="600"
          size="20.82px"
          color={theme.royalBlue}
        >
          {post?.title}
        </NewsPara>
        <Margin margin="10px 0 24px 26px">
          <NewsHeader
            upload={post?.date}
            popular={post?.view}
            start
            date="14.24px"
            view="13.29px"
          />
        </Margin>
        {post?.imgsm ? (
          <Image
            src={post.imgsm}
            layout="responsive"
            width={776.3}
            height={527.19}
          ></Image>
        ) : (
          ""
        )}
        {post?.content?.map((v: string, i: number) => {
          return (
            <DetailPara
              family={theme.OpenSans}
              size="14.58px"
              margin="25px 0 0"
              lineHeight="1.7"
            >
              {v}
            </DetailPara>
          );
        })}
      </DetailContainer>
    </Right>
  );
};

export default MainSection;
