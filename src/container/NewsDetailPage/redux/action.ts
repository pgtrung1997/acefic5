import { apiURL } from "./../../../shared/ultis/url";
import * as Types from "./action-type";
import { get } from "../../../shared/ultis/axios";

export const getNewsPost = async (dispatch: any, postId: string) => {
  try {
    const res = await get(apiURL.newsdetailpage.posts + `/${postId}`);
    if (res) {
      dispatch({ type: Types.RECEIVE_NEWSPOST, payload: res });
    }
  } catch {}
};
