import * as Types from "./action-type";

const initialState = {
  post: [],
};

export const NewsDetailpageReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case Types.RECEIVE_NEWSPOST:
      const post = action.payload;
      return { ...state, post };
    default:
      return state;
  }
};
